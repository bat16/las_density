@ECHO OFF
setlocal enabledelayedexpansion
ECHO Batch script to generate laser density export from LAS to ASC and TIFF
ECHO Author: Marek Szczepkowski
ECHO Date: 18-04-2020
ECHO Version: 1.0
ECHO For SAGA 6.0, LASTool, QGIS 2.14
ECHO.


REM CREATE LEGEND Value;R;G;B;Alfa
REM nv(no_value color)
(
echo nv 0 0 0 0 
echo 0 255 0 0 255 
echo 6 255 0 0 255
echo 7 0 255 0 255
echo 255 0 255 0 255 ) > Density_style.txt

REM ROOTS_USER_DEFINED:
SET SAGA_ROOT="d:\MSZ\__SCRIPTS, COMMANDS\DTM_NDSM\DTN_DSM_nDSM\release_1.0.0\saga-6.0.0_x64\"
SET QGIS_ROOT="C:\Program Files\QGIS 2.14"



SET PATH=%PATH%;%SAGA_ROOT%
SET PATH=%PATH%;%QGIS_ROOT%\bin
SET GDAL_DATA=%QGIS_ROOT%\share\gdal
SET WORK=%cd%
SET OUT_SUFFIX=_density


REM COUNTER FILES
dir /b *.las 2> nul | find "" /v /c > tmp && set /p countLAS=<tmp && del tmp

set /A Counter=1

FOR /F %%i IN ('dir /b "%WORK%\*.las"') DO (
	ECHO. 
	ECHO. Processing  %%i....    !Counter! / %countLAS% FILES
	
	REM Read RGB, return number, number of returns of given point
	REM RGB_RANGE = 16 bit, -n = import "number of returns of given point" -valid -r = import return number = import only points consiederd valid
	saga_cmd --flags=r io_shapes_las "Import LAS Files" -FILES="%WORK%\%%i" -POINTS="%WORK%\%%i" -RGB_RANGE:0 -c:1 -VALID:0 
	ECHO. GRID   
	saga_cmd --flags=r pointcloud_tools "Point Cloud to Grid" -POINTS:"%WORK%\%%~ni.sg-pts"  -OUTPUT:0 -AGGREGATION:0 -CELLSIZE=1.000000 -COUNT:"%WORK%\%%~ni_density.sgrd"
	ECHO. SAVE
	saga_cmd --flags=r io_grid "Export ESRI Arc/Info Grid" -GRID:"%WORK%\%%~ni_density.sgrd" -FILE:"%WORK%\%%~ni%OUT_SUFFIX%.asc" -FORMAT:1 -GEOREF:0 -PREC:4 -DECSEP:0 
	ECHO. COLOR
	gdaldem color-relief -of GTiff -co "TFW=YES" -alpha -co ALPHA=YES "%WORK%\%%~ni%OUT_SUFFIX%.asc" "Density_style.txt" "%WORK%\%%~ni%OUT_SUFFIX%_color.tif" 
	
    DEL /Q "%WORK%\*.prj"
    DEL /Q "%WORK%\*.mgrd"
    DEL /Q "%WORK%\*.sdat" 
    DEL /Q "%WORK%\*.sgrd"
    DEL /Q "%WORK%\*.sg-pts"
    DEL /Q "%WORK%\*.sg-info"
    DEL /Q "%WORK%\*.mshp"

	set /A Counter+=1	
)
DEL /Q "%WORK%\Density_style.txt"
ECHO.
ECHO.
ECHO. SCRIPT ENDS WITHOUT ERRORS

PAUSE