@ECHO OFF
setlocal enabledelayedexpansion
ECHO Batch script to generate lower laser density SHP than value
ECHO Author: Marek Szczepkowski
ECHO Date: 17-05-2020
ECHO Version: 1.0
ECHO For SAGA 6.0
ECHO.


REM ROOTS_USER_DEFINED:
SET SAGA_ROOT="d:\MSZ\__SCRIPTS_COMMANDS\DTM_NDSM\DTN_DSM_nDSM\release_1.0.0\saga-6.0.0_x64\"
SET QGIS_ROOT="C:\Program Files\QGIS 2.14"
SET MIN=50
SET GRID=1
SET MERGE=YES


REM Set Paths
SET PATH=%PATH%;%SAGA_ROOT%
SET PATH=%PATH%;%QGIS_ROOT%\bin
SET GDAL_DATA=%QGIS_ROOT%\share\gdal
SET WORK=%cd%
if not exist "%WORK%\_TEMP" mkdir "%WORK%\_TEMP"
SET OUT_SUFFIX=_density


REM Create style
(
echo nv 0 0 0 255 
echo 1 255 0 0 255 
echo 2 0 255 0 255
 ) > "%WORK%\_TEMP\Density_style.txt"


REM Counter Files
dir /b *.las 2> nul | find "" /v /c > tmp && set /p countLAS=<tmp && del tmp


REM Main
set /A Counter=1
FOR /F %%i IN ('dir /b "%WORK%\*.las"') DO (
	ECHO. 
	ECHO. Processing  %%i....    !Counter! / %countLAS% FILES
	
	ECHO. LOAD
	saga_cmd --flags=r io_shapes_las "Import LAS Files" -FILES="%WORK%\%%i" -POINTS="%WORK%\_TEMP\%%i" -VALID:0 

	ECHO.	
	ECHO. GRID   
	saga_cmd --flags=r pointcloud_tools "Point Cloud to Grid" -POINTS:"%WORK%\_TEMP\%%~ni.sg-pts"  -OUTPUT:0 -AGGREGATION:0 -CELLSIZE=%GRID% -COUNT:"%WORK%\_TEMP\%%~ni_density.sgrd"
	
	ECHO.	
	ECHO. RECLASSIFY GRID 
	saga_cmd --flags=r grid_tools "Reclassify Grid Values" -INPUT:"%WORK%\_TEMP\%%~ni_density.sgrd" -RESULT="%WORK%\_TEMP\%%~ni_reclass.sgrd"  -METHOD=1 -MIN=0 -MAX=%MIN% -RNEW=1 -ROPERATOR=1 -NODATAOPT=1 -NODATA=-99999 -OTHEROPT=1 -OTHERS=2 -RESULT_NODATA_CHOICE=1 -RESULT_NODATA_VALUE=-99999

	ECHO.	
	ECHO. SAVE
	saga_cmd --flags=r shapes_grid "Vectorising Grid Classes" -GRID="%WORK%\_TEMP\%%~ni_reclass.sgrd" -POLYGONS="%WORK%\_TEMP\%%~ni_wrong_density.shp" -CLASS_ALL=0 -CLASS_ID=1.000000 -SPLIT=1 -ALLVERTICES=0
	saga_cmd --flags=r io_grid "Export ESRI Arc/Info Grid" -GRID:"%WORK%\_TEMP\%%~ni_reclass.sgrd" -FILE:"%WORK%\_TEMP\%%~ni%OUT_SUFFIX%.asc" -FORMAT:1 -GEOREF:0 -PREC:4 -DECSEP:0 
	
	ECHO.
	ECHO. COLOR TIFF
	gdaldem color-relief -of GTiff -co "TFW=YES" -alpha -co ALPHA=YES "%WORK%\_TEMP\%%~ni%OUT_SUFFIX%.asc" "Density_style.txt" "%WORK%\_TEMP\%%~ni%OUT_SUFFIX%.tif" 

    DEL /Q "%WORK%\_TEMP\%%~ni_density.prj"
    DEL /Q "%WORK%\_TEMP\%%~ni_reclass.prj"
    DEL /Q "%WORK%\_TEMP\*.mgrd"
    DEL /Q "%WORK%\_TEMP\*.sdat" 
    DEL /Q "%WORK%\_TEMP\*.sgrd"
    DEL /Q "%WORK%\_TEMP\*.sg-pts"
    DEL /Q "%WORK%\_TEMP\*.sg-info"
    DEL /Q "%WORK%\_TEMP\*.mshp"
	DEL /Q "%WORK%\_TEMP\*.asc"
	set /A Counter+=1	
)
DEL /Q "%WORK%\_TEMP\Density_style.txt"


REM Merge of move files
IF "%MERGE%" == "YES" (
	ECHO.
	ECHO. MERGING FILES
	FOR /F %%i IN ('dir /b "%WORK%\_TEMP\*.shp"') do (
	  ECHO 	ADDING %%i
	  ogr2ogr -update -append -f "ESRI Shapefile" "%WORK%\Density_merged.shp" "%WORK%\_TEMP\%%i"
	  set /A Counter2+=1	)
	gdalwarp -of GPKG "%WORK%\_TEMP\*.tif" "%WORK%\_TEMP\Merged.gpkg"
	gdal_translate -of GTiff -co "TFW=YES" "%WORK%\_TEMP\Merged.gpkg" "%WORK%\Density_merged.tif"
) ELSE (
move "%WORK%\_temp\*.*" "%WORK%" > null
)


REM DELETE TEMP
rmdir "%WORK%\_TEMP\" /Q/S


REM FINISH
ECHO.
ECHO.
ECHO. SCRIPT ENDS WITHOUT ERRORS
PAUSE

