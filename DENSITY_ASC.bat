@ECHO OFF
setlocal enabledelayedexpansion
ECHO Batch script to generate laser density export
ECHO Terrain model: NMT
ECHO Author: Marek Szczepkowski
ECHO Date: 11-10-2019
ECHO Version: 1.3
ECHO For SAGA 6.0, LASTool, QGIS 2.14
ECHO.

SET SAGA_ROOT="d:\MSZ\__SCRIPTS, COMMANDS\DTM_NDSM\DTN_DSM_nDSM\release_1.0.0\saga-6.0.0_x64\"
SET QGIS_ROOT="C:\Program Files\QGIS 2.14"
SET LASTOOLS_ROOT="d:\MSZ\__SCRIPTS, COMMANDS\DTM_NDSM\DTN_DSM_nDSM\release_1.0.0\LAStools\"

REM Setup saga_cmd.exe
SET PATH=%PATH%;%SAGA_ROOT%
REM Setup gdal_transalte.exe
SET PATH=%PATH%;%QGIS_ROOT%\bin
SET GDAL_DATA=%QGIS_ROOT%\share\gdal
REM Setup txt2las.exe
SET xyz2las=%LASTOOLS_ROOT%\bin\txt2las.exe

REM Path to working dir
SET WORK=%cd%
REM Suffix output files
SET OUT_SUFFIX=_density

REM COUNTER FILES
dir /b *.las 2> nul | find "" /v /c > tmp && set /p countLAS=<tmp && del tmp



set /A Counter=1

FOR /F %%i IN ('dir /b "%WORK%\*.las"') DO (
	ECHO. 
	ECHO.
	ECHO Processing  %%i....    !Counter! / %countLAS% FILES
	
	REM Read RGB, return number, number of returns of given point
	REM RGB_RANGE = 16 bit, -n = import "number of returns of given point" -valid -r = import return number = import only points consiederd valid
	saga_cmd --flags=r io_shapes_las "Import LAS Files" -FILES="%WORK%\%%i" -POINTS="%WORK%\%%i" -RGB_RANGE:0 -c:1 -VALID:0 
	ECHO GRID   
	saga_cmd --flags=r pointcloud_tools "Point Cloud to Grid" -POINTS:"%WORK%\%%~ni.sg-pts"  -OUTPUT:0 -AGGREGATION:0 -CELLSIZE=1.000000 -COUNT:"%WORK%\%%~ni_density.sgrd"
	ECHO SAVE
	saga_cmd --flags=r io_grid "Export ESRI Arc/Info Grid" -GRID:"%WORK%\%%~ni_density.sgrd" -FILE:"%WORK%\%%~ni%OUT_SUFFIX%.asc" -FORMAT:1 -GEOREF:0 -PREC:4 -DECSEP:0 

	ECHO COLOR
	gdaldem color-relief -of GTiff -co "TFW=YES" -alpha -co ALPHA=YES "%WORK%\%%~ni%OUT_SUFFIX%.asc" "Density_style.txt" "%WORK%\%%~ni%OUT_SUFFIX%_color.tif" 
	
    DEL /Q "%WORK%\*.prj"
    DEL /Q "%WORK%\*.mgrd"
    DEL /Q "%WORK%\*.sdat" 
    DEL /Q "%WORK%\*.sgrd"
    DEL /Q "%WORK%\*.sg-pts"
    DEL /Q "%WORK%\*.sg-info"


	set /A Counter+=1	
)
ECHO SCRIPT ENDS WITHOUT ERRORS

PAUSE